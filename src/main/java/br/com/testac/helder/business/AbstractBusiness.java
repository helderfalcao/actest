package br.com.testac.helder.business;

import java.util.List;

import br.com.testac.helder.dao.AbstractDao;
import br.com.testac.helder.model.BaseModel;

public interface AbstractBusiness<T extends AbstractDao, E extends BaseModel> {

	E findByid(Long id);

	void save(E entity);

	void update(E entity);

	List<E> lista();

	void remove(Long entityId);

}
