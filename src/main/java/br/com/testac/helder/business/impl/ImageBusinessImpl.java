package br.com.testac.helder.business.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.testac.helder.dao.ImageDao;
import br.com.testac.helder.model.Image;

@Component
public class ImageBusinessImpl extends AbstractBusinessImpl<ImageDao, Image> {

	@Autowired
	private ImageDao imageDao;

	@Override
	protected ImageDao getDao() {
		return imageDao;
	}
	
}
