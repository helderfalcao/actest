package br.com.testac.helder.business;

import br.com.testac.helder.dao.ProductDao;
import br.com.testac.helder.model.Product;

public interface ProductBusiness extends AbstractBusiness<ProductDao, Product> {

}
