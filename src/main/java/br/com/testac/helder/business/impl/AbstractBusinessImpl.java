package br.com.testac.helder.business.impl;

import java.util.List;

import org.springframework.stereotype.Component;

import br.com.testac.helder.dao.AbstractDao;
import br.com.testac.helder.model.BaseModel;

@Component
@SuppressWarnings("unchecked")
public abstract class AbstractBusinessImpl<T extends AbstractDao, E extends BaseModel> {

	public E findByid(Long id, List<String> fetchProperties) {
		return (E) getDao().findById(id, fetchProperties);
	}

	public void save(E entity) {
		getDao().save(entity);
	}

	public void update(E entity) {
		getDao().update(entity);
	}

	public List<E> list(List<String> fetchProperties) {
		return getDao().lista(fetchProperties);
	}

	public void remove(Long entityId) {
		getDao().remove(entityId);
	}

	protected abstract T getDao();

}
