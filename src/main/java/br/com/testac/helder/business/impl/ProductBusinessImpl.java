package br.com.testac.helder.business.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.testac.helder.dao.ProductDao;
import br.com.testac.helder.model.Product;

@Component
public class ProductBusinessImpl extends AbstractBusinessImpl<ProductDao, Product> {

	@Autowired
	private ProductDao productDao;
	
	@Override
	protected ProductDao getDao() {
		return productDao;
	}

}
