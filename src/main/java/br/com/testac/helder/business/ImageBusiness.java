package br.com.testac.helder.business;

import br.com.testac.helder.dao.ImageDao;
import br.com.testac.helder.model.Image;

public interface ImageBusiness extends AbstractBusiness<ImageDao, Image> {

}
