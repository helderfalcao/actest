package br.com.testac.helder.service.impl;

import java.util.Collections;
import java.util.List;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.testac.helder.business.impl.ProductBusinessImpl;
import br.com.testac.helder.dao.LazyUtils;
import br.com.testac.helder.model.Product;
import br.com.testac.helder.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductBusinessImpl productBusiness;

	@Override
	public Product getEntity(Long id, List<String> fetchPropertyes) {
		Product product = productBusiness.findByid(id, fetchPropertyes);
		removeLazyValues(Collections.singletonList(product));
		return product;
	}

	@Override
	public List<Product> getEntityList(List<String> fetchPropertyes) {
		List<Product> products = productBusiness.list(fetchPropertyes);
		removeLazyValues(products);
		return products;

	}

	@Override
	public Product updateEntity(Product entity) {
		productBusiness.update(entity);
		removeLazyValues(Collections.singletonList(entity));
		return entity;
	}

	@Override
	public Product saveEntity(Product entity) {
		productBusiness.save(entity);
		removeLazyValues(Collections.singletonList(entity));
		return entity;
	}

	@Override
	public void deleteEntity(Long id) {
		productBusiness.remove(id);
	}

	private void removeLazyValues(List<Product> products) {
		LazyUtils.removeLazyNotLoadedValuesFromProducts(products);
	}

}
