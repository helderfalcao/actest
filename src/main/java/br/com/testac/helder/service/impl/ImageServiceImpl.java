package br.com.testac.helder.service.impl;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.testac.helder.business.impl.ImageBusinessImpl;
import br.com.testac.helder.dao.LazyUtils;
import br.com.testac.helder.model.Image;
import br.com.testac.helder.model.Product;
import br.com.testac.helder.service.ImageService;

@Service
public class ImageServiceImpl implements ImageService {

	@Autowired
	private ImageBusinessImpl imageBusiness;

	@Override
	public Image getEntity(Long id, List<String> fetchPropertyes) {
		Image image = imageBusiness.findByid(id, fetchPropertyes);
		removeLazyValues(Collections.singletonList(image));
		return image;
	}

	@Override
	public List<Image> getEntityList(List<String> fetchPropertyes) {
		List<Image> images = imageBusiness.list(fetchPropertyes);
		removeLazyValues(images);
		return images;
	}

	@Override
	public Image updateEntity(Image entity) {
		imageBusiness.update(entity);
		removeLazyValues(Collections.singletonList(entity));
		return entity;
	}

	@Override
	public Image saveEntity(Image entity) {
		imageBusiness.save(entity);
		removeLazyValues(Collections.singletonList(entity));
		return null;
	}

	@Override
	public void deleteEntity(Long id) {
		imageBusiness.remove(id);
	}

	private void removeLazyValues(List<Image> images) {
		LazyUtils.removeLazyValuesFromImage(images);
	}

}
