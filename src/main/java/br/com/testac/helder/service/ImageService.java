package br.com.testac.helder.service;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import br.com.testac.helder.model.Image;

@Path("/image")
public interface ImageService {

	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	Image getEntity(@PathParam("id") Long id, @QueryParam("fetch_property") List<String> fetchPropertyes);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	List<Image> getEntityList(@QueryParam("fetch_property") List<String> fetchPropertyes);

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	Image updateEntity(Image image);

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	Image saveEntity(Image entity);
	
	@DELETE
	@Path("{id}")
	void deleteEntity(@PathParam("id")Long id);
}
