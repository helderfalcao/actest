package br.com.testac.helder;

import java.util.Collections;

import org.apache.cxf.endpoint.Server;
import org.glassfish.jersey.jackson.internal.jackson.jaxrs.json.JacksonJsonProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.endpoint.MetricReaderPublicMetrics;
import org.springframework.boot.actuate.endpoint.MetricsEndpoint;
import org.springframework.boot.actuate.metrics.reader.MetricRegistryMetricReader;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import com.codahale.metrics.MetricRegistry;

@SpringBootApplication
public class HelderApplication {

	@Autowired
	private ApplicationContext applicationContext;

	@Autowired
	Server server;

	@Bean
	public MetricRegistry metricRegistry() {
		return new MetricRegistry();
	}

	@Bean
	public MetricsEndpoint metricsEndpoint(final MetricRegistry registry) {
		return new MetricsEndpoint(
				Collections.singleton(new MetricReaderPublicMetrics(new MetricRegistryMetricReader(registry))));
	}
	
	@Bean
	public JacksonJsonProvider jsonProvider() {
		return new JacksonJsonProvider();
	}

	public static void main(String[] args) {
		SpringApplication.run(HelderApplication.class, args);
	}

}
