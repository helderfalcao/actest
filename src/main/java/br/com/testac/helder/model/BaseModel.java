package br.com.testac.helder.model;

public abstract class BaseModel {

	public abstract Long getId();
	
	public abstract void setId(Long id);
	
}
