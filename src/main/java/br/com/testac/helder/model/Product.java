package br.com.testac.helder.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.Getter;
import lombok.Setter;

@Entity
public class Product extends BaseModel {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Getter
	@Setter
	private Long id;

	@Getter
	@Setter
	@ManyToOne
	@JoinColumn(name = "parent")
	private Product parent;

	@Getter
	@Setter
	@OneToMany(mappedBy = "product")
	private List<Image> image;
	
	@Getter
	@Setter
	@OneToMany(mappedBy = "parent")
	private List<Product> childs;

}
