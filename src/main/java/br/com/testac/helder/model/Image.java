package br.com.testac.helder.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.Setter;

@Entity
public class Image extends BaseModel implements Serializable {

	private static final long serialVersionUID = 4204045085281385571L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Getter
	@Setter
	private Long id;

	@Getter
	@Setter
	@Column(name = "path")
	private String path;

	@Getter
	@Setter
	@Column(name = "size")
	private Long size;

	@ManyToOne
	@JoinColumn(name = "product")
	@Getter
	@Setter
	private Product product;
}
