package br.com.testac.helder.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;
import br.com.testac.helder.model.BaseModel;

@Repository
public abstract class AbstractDao<T extends BaseModel> {

	@PersistenceContext
	private EntityManager entityManager;

	protected abstract Class<T> entityClazz();

	@Transactional
	public void save(T entity) {
		entityManager.persist(entity);
	}

	@Transactional
	public void update(T entity) {
		entityManager.merge(entity);
	}

	public List<T> lista(List<String> atributesToFetch) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<T> cq = cb.createQuery(entityClazz());
		Root<T> c = fetchProperties(atributesToFetch, cq);
		TypedQuery<T> query = entityManager.createQuery(cq.select(c));
		return query.getResultList();
	}

	public T findById(Long id, List<String> atributesToFetch) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<T> cq = cb.createQuery(entityClazz());
		Root<T> c = fetchProperties(atributesToFetch, cq);
		cq.where(cb.equal(c.get("id"), id));
		TypedQuery<T> query = entityManager.createQuery(cq.select(c));
		return query.getSingleResult();
	}

	@Transactional
	public void remove(Long entityId) {
		T entityToRemove = findById(entityId, null);
		entityManager.remove(entityToRemove);
	}

	private Root<T> fetchProperties(List<String> atributesToFetch, CriteriaQuery<T> cq) {
		Root<T> c = cq.from(entityClazz());
		if (atributesToFetch != null) {
			atributesToFetch.stream().forEach(att -> {
				c.fetch(att);
			});
		}
		return c;
	}

}
