package br.com.testac.helder.dao;

import java.util.List;

import org.hibernate.Hibernate;

import br.com.testac.helder.model.Image;
import br.com.testac.helder.model.Product;

public class LazyUtils {

	public static void removeLazyValuesFromImage(List<Image> images) {
		if (images != null) {
			images.stream().forEach(item -> {
				removeLazyValues(item.getProduct());
			});
		}
	}

	public static void removeLazyNotLoadedValuesFromProducts(List<Product> products) {
		if (products != null) {
			products.stream().forEach(item -> {
				if (!Hibernate.isInitialized(item.getChilds())) {
					item.setChilds(null);
				}
				if (Hibernate.isInitialized(item.getImage())) {
					removeLazyValuesFromImage(item.getImage());
				} else {
					item.setImage(null);
				}
				removeLazyValues(item.getParent());
			});
		}
	}

	private static void removeLazyValues(Product product) {
		if (product != null) {
			product.setChilds(null);
			product.setParent(null);
		}
	}

}
