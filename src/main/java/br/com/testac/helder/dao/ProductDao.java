package br.com.testac.helder.dao;

import org.springframework.stereotype.Repository;

import br.com.testac.helder.model.Product;

@Repository
public class ProductDao extends AbstractDao<Product> {

	@Override
	protected Class<Product> entityClazz() {
		return Product.class;
	}

}
