package br.com.testac.helder.dao;

import org.springframework.stereotype.Repository;

import br.com.testac.helder.model.Image;

@Repository
public class ImageDao extends AbstractDao<Image> {

	@Override
	protected Class<Image> entityClazz() {
		return Image.class;
	}

}
